// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;


UENUM()//���� ��� ������ �� ��������
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	UPPER,
	DOWNER
};
 
UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;//����������� ��������� ������� ������� ����� ��������� � ����� ����� ����

	UPROPERTY(EditDefaultsOnly)// ������ �������� �������������� �� ���������
	float ElementSize;



	UPROPERTY(EditDefaultsOnly)
	TArray<ASnakeElementBase*> SnakeElements;//����������� �������

	UPROPERTY()
	EMovementDirection LastMoveDirection;// ������� ������������ ����������� ��������

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;// ������� ������������ ��������

	FVector SnakeHeadLocat;

	FVector SnakeHeadBoostLocat;
	
	FVector SnakeBoostStep;
	
	int32 HowMuchFoodEaten;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void DecriseSnakeElement();

	void AddSnakeElement(int ElementsNum = 1);// ������� ����������� � ����� snakeBaseElement

	
	void Move();// ������� ��������
	
	void MoveBoost();

	void SnakeCameraTransform();

	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other); // ������� ������� ���������� �������

	void PlusValueHowMuchFoodEaten();
	int Geteaten();
};
