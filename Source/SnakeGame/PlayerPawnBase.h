// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ASnakeElementBase;
class AFoodDecrise;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 HowMuchFood;

	UPROPERTY()
	TArray<AFood*> FoodElements;//определение массива

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodDecrise> FoodDecriseActorClass;
	
	UPROPERTY()
	TArray<AFoodDecrise*> FoodDecriseElements;//определение массива

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElementsProps;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeActorPropsClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 HowMuchProps;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 HowMuchFoodDecrise;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void EatenBool(bool eaten);


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void AddFoodElements();

	void AddPropsElements();

	void AddFoodDecriseElements();

	void CreateSnakeActor();

	void CreateFoodActor();



	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlePlayerUpDownInput(float value);
	UFUNCTION()
	void HandlePlayerBoostInput(float value);
};
